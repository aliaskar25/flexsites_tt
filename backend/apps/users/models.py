import os

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

from .utils import generate_random_filename, save_resized_image


class CustomUserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password=None, **extra_fields):
        if not email:
            raise ValueError("The Email field must be set")
        if not first_name:
            raise ValueError("The First Name field must be set")
        if not last_name:
            raise ValueError("The Last Name field must be set")

        email = self.normalize_email(email)
        user = self.model(email=email, first_name=first_name, last_name=last_name, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        return self.create_user(email, first_name, last_name, password, **extra_fields)
    

class Organization(models.Model):
    name = models.CharField(max_length=255)
    short_description = models.TextField()

    # class Meta:
    #     app_label = 'users'


class CustomUser(AbstractUser):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    phone = models.CharField(max_length=15)
    avatar = models.ImageField(upload_to=generate_random_filename, blank=True, null=True)
    organizations = models.ManyToManyField(Organization, related_name='users')

    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = CustomUserManager()

    # class Meta:
    #     app_label = 'users'

    def save(self, *args, **kwargs):
        if self.pk:
            old_instance = CustomUser.objects.get(pk=self.pk)
            if old_instance.avatar != self.avatar and os.path.isfile(old_instance.avatar.path):
                os.remove(old_instance.avatar.path)
        super(CustomUser, self).save(*args, **kwargs)
        save_resized_image(self)
