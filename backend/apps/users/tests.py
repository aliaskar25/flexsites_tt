import os
from io import BytesIO
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from .models import CustomUser, Organization


def create_test_image():
    image = Image.new('RGB', (200, 200), color='red')
    img_io = BytesIO()
    image.save(img_io, format='JPEG')
    img_io.seek(0)
    return InMemoryUploadedFile(
        img_io, field_name=None, name='test_image.jpg', content_type='image/jpeg', size=img_io.tell, charset=None
    )


class CustomUserTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user_data = {
            'email': 'test@example.com',
            'password': 'test_password',
            'last_name': 'Doe',
            'first_name': 'John',
            'phone': '+1234567890',
            'avatar': create_test_image(),
        }
        self.user = CustomUser.objects.create_user(**self.user_data)

    def test_create_user(self):
        response = self.client.post(reverse('user-list'), self.user_data, format='multipart')
        self.assertEqual(response.status_code, 201)
        self.assertTrue(CustomUser.objects.filter(email='test@example.com').exists())

    def test_retrieve_user(self):
        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['email'], 'test@example.com')

    def test_update_user(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(
            reverse('user-detail', kwargs={'pk': self.user.pk}),
            {'last_name': 'NewLastName'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 200)
        self.user.refresh_from_db()
        self.assertEqual(self.user.last_name, 'NewLastName')

    def test_unauthorized_update_user(self):
        response = self.client.patch(
            reverse('user-detail', kwargs={'pk': self.user.pk}),
            {'last_name': 'NewLastName'},
            format='multipart'
        )
        self.assertEqual(response.status_code, 401)

    def test_list_users(self):
        response = self.client.get(reverse('user-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_delete_user_not_allowed(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(reverse('user-detail', kwargs={'pk': self.user.pk}))
        self.assertEqual(response.status_code, 405)  # Method not allowed

    def test_update_organization_not_allowed(self):
        response = self.client.patch(
            reverse('organization-detail', kwargs={'pk': self.organization.pk}),
            {'name': 'New Organization Name'}
        )
        self.assertEqual(response.status_code, 405)  # Method not allowed

    def test_delete_organization_not_allowed(self):
        response = self.client.delete(reverse('organization-detail', kwargs={'pk': self.organization.pk}))
        self.assertEqual(response.status_code, 405)  # Method not allowed


class OrganizationTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.organization_data = {
            'name': 'Test Organization',
            'description': 'Test description',
        }
        self.organization = Organization.objects.create(**self.organization_data)

    def test_create_organization(self):
        response = self.client.post(reverse('organization-list'), self.organization_data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Organization.objects.filter(name='Test Organization').exists())

    def test_retrieve_organization(self):
        response = self.client.get(reverse('organization-detail', kwargs={'pk': self.organization.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], 'Test Organization')

    def test_list_organizations(self):
        response = self.client.get(reverse('organization-list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
