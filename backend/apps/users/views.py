from django.contrib.auth import get_user_model

from rest_framework import generics
from rest_framework_simplejwt.views import TokenObtainPairView

from .models import Organization
from .serializers import (
    CustomUserSerializer, OrganizationSerializer, 
    CustomUserLoginSerializer, 
)
from .permissions import IsOwner


class CustomUserRegister(generics.CreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = CustomUserSerializer


class CustomUserLogin(TokenObtainPairView):
    serializer_class = CustomUserLoginSerializer


class CustomUserDetail(generics.RetrieveUpdateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = CustomUserSerializer
    lookup_field = 'id'
    permission_classes = [IsOwner]


class CustomUserList(generics.ListAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = CustomUserSerializer


class OrganizationList(generics.ListCreateAPIView):
    queryset = Organization.objects.all().prefetch_related('users')
    serializer_class = OrganizationSerializer


class OrganizationDetail(generics.RetrieveAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    lookup_field = 'id'
