from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from .models import Organization


class CustomUserInOrganization(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'first_name', 'last_name', 'email']


class OrganizationSerializer(serializers.ModelSerializer):
    users = CustomUserInOrganization(many=True)

    class Meta:
        model = Organization
        fields = ['id', 'name', 'short_description', 'users']


class CustomUserSerializer(serializers.ModelSerializer):
    organizations = OrganizationSerializer(many=True, read_only=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = get_user_model()
        fields = [
            'id', 'email', 'first_name', 'last_name', 
            'password', 'phone', 'avatar', 'organizations'
        ]
    
    def create(self, validated_data):
        password = validated_data.pop('password')  # Извлеките пароль из validated_data
        user = get_user_model()(**validated_data)
        user.set_password(password)  # Устанавливаем пароль с помощью set_password
        user.save()
        return user


class CustomUserLoginSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        data.update({'user': CustomUserSerializer(self.user).data})
        return data
