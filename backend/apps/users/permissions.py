from rest_framework import permissions

class IsOwner(permissions.BasePermission):
    """
    Кастомное разрешение, которое позволяет только владельцам объекта изменять его.
    """

    def has_object_permission(self, request, view, obj):
        # Разрешения на чтение предоставляются всем, поэтому GET, HEAD и OPTIONS запросы разрешены.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Запись разрешена только владельцу объекта.
        return obj == request.user
