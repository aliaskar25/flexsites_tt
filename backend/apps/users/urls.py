from django.urls import path
from . import views

urlpatterns = [
    path('users/register/', views.CustomUserRegister.as_view(), name='register'),
    path('users/login/', views.CustomUserLogin.as_view(), name='login'),
    path('users/<int:id>/', views.CustomUserDetail.as_view(), name='user-detail'),
    path('users/', views.CustomUserList.as_view(), name='user-list'),
    path('organizations/<int:id>/', views.OrganizationDetail.as_view(), name='organization-detail'),
    path('organizations/', views.OrganizationList.as_view(), name='organization-list'),
]
