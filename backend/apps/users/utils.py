import os
import random
import string
from django.conf import settings
from PIL import Image
from io import BytesIO


def generate_random_filename(instance, filename):
    extension = os.path.splitext(filename)[1]
    new_filename = ''.join(random.choices(string.ascii_letters + string.digits, k=10)) + extension
    upload_path = os.path.join('avatars', new_filename)
    os.makedirs(os.path.join(settings.MEDIA_ROOT, 'avatars'), exist_ok=True)
    return upload_path


def save_resized_image(instance):
    if instance.avatar:
        img = Image.open(instance.avatar.path)
        img.thumbnail((200, 200))

        img_io = BytesIO()
        img.save(img_io, format=img.format, quality=90)

        with open(instance.avatar.path, 'wb') as f:
            f.write(img_io.getvalue())
